﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

    [SerializeField]
    private Transform playerCamera;
    [SerializeField]
    private float speed;
    [SerializeField]
    private float jumpPower;
    [SerializeField]
    private GameObject ballPrefab;
    //private Rigidbody _rigidbody;

    void Awake()
    {
       // _rigidbody = GetComponent<Rigidbody>();
    }

	// Use this for initialization
	void Start () {
	}
	
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            Jump();
        }
        if(Input.GetMouseButtonDown(0))
        {
            //Throw();
            Shoot();
        }
    }

	// Update is called once per frame
	void FixedUpdate () {
        Move();
	}

    private void Move()
    {
        float x =  Mathf.Clamp(Input.GetAxis("Horizontal"), -1, 1);
        float z = Mathf.Clamp(Input.GetAxis("Vertical"), -1, 1);

        Vector3 newVelocity = new Vector3(x*speed, rigidbody.velocity.y ,z*speed);
        rigidbody.velocity = newVelocity;
    }

    private void Jump()
    {
        Vector3 jump = transform.up * jumpPower;
        Debug.Log("Jump: " + jump);
        rigidbody.AddForce(jump);
    }

    private void Throw()
    {
        GameObject ball = Instantiate(ballPrefab, transform.position, Quaternion.identity) as GameObject;
        ball.rigidbody.AddForce(ball.transform.forward * jumpPower);
    }

    private void Shoot()
    {
        RaycastHit hitInfo;
        if(Physics.Raycast(transform.position, playerCamera.transform.forward, out hitInfo))
        {
            Target currentTarget = hitInfo.transform.GetComponent<Target>();
            if(currentTarget != null)
            {
                currentTarget.HitMe();
            }
        }
    }
}
