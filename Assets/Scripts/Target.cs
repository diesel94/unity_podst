﻿using UnityEngine;
using System.Collections;

public class Target : MonoBehaviour {

    private int hp;
    private Material material;

    void Awake()
    {
        material = renderer.material;
        hp = 100;
    }

    void Start()
    {
        material.color = Color.green;
    }

	public void HitMe()
    {
        hp -= 10;
        Debug.Log(name + " hp: " + hp);
        if (hp <= 0)
        {
            KillMe();
        }
    }

    public void ChangeColor(Color color)
    {
        material.color = color;
    }

    private void KillMe()
    {
        material.color = Color.red;
        Destroy(gameObject, 0.25f);
    }
}
