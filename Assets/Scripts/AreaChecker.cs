﻿using UnityEngine;
using System.Collections;

public class AreaChecker : MonoBehaviour {

    [SerializeField]
    private Target target;

    void OnTriggerEnter(Collider coll)
    {
        if(coll.tag.Equals("Player"))
        {
            target.ChangeColor(Color.yellow);
        }
    }

    void OnTriggerStay(Collider coll)
    {
        if (coll.tag.Equals("Player"))
        {
            target.ChangeColor(Color.blue);
        }
    }

    void OnTriggerExit(Collider coll)
    {
        if (coll.tag.Equals("Player"))
        {
            target.ChangeColor(Color.green);
        }
    }
}
